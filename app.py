import os
from dotenv import find_dotenv, load_dotenv
from transformers import pipeline

load_dotenv(find_dotenv())
HUGGINGFACEHUB_API_TOKEN=os.getenv("HUGGINGFACEHUB_API_TOKEN")

def image2text(url):
    image_to_text= pipeline("image-to-text", model="Salesforce/blip-image-captioning-base")

    text=image_to_text(
        url)[0]['generated_text']
    
    print(text)
    return text
